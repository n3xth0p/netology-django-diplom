from django_filters import rest_framework as filters
from products.models import Product


class ProductFilter(filters.FilterSet):
    """ Фильтр для товаров """
    description = filters.CharFilter(field_name='description', lookup_expr='icontains')
    title = filters.CharFilter(field_name='title', lookup_expr='icontains')

    price_gt = filters.NumberFilter(field_name='price', lookup_expr='gt')
    price_lt = filters.NumberFilter(field_name='price', lookup_expr='lt')

    class Meta:
        model = Product
        fields = ('price', 'description', 'title')
