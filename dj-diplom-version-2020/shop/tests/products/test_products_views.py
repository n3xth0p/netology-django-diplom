import pytest
from rest_framework.reverse import reverse
from rest_framework.status import HTTP_200_OK, \
    HTTP_201_CREATED, \
    HTTP_403_FORBIDDEN, \
    HTTP_204_NO_CONTENT, \
    HTTP_401_UNAUTHORIZED


class TestProductViewSet:

    def set_up(self):
        pass

    @pytest.mark.django_db
    def test_retreive_product_list(self, api_client):
        url = reverse('products-list')
        resp = api_client.get(url)
        assert resp.status_code == HTTP_200_OK

    @pytest.mark.parametrize(
        ["is_superuser", "http_response"],
        (
                (True, HTTP_200_OK),
                (False, HTTP_200_OK)
        )
    )
    @pytest.mark.django_db
    def test_retreive_product_details(self, api_client, products_factory, user_factory, token_factory, is_superuser,
                                      http_response):
        user = user_factory(is_superuser=is_superuser)
        token = token_factory(user=user)
        products = products_factory(_quantity=1)
        url = reverse('products-detail', args=[products[0].id])
        api_client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        resp = api_client.get(url)
        assert resp.status_code == http_response

    @pytest.mark.parametrize(
        ["is_superuser", "http_response"],
        (
                (True, HTTP_201_CREATED),
                (False, HTTP_403_FORBIDDEN)
        )
    )
    @pytest.mark.django_db
    def test_add_product(self, api_client, products_factory, user_factory, token_factory, is_superuser, http_response):
        user = user_factory(is_superuser=is_superuser)
        token = token_factory(user=user)
        url = reverse('products-list')

        product_payload = {
            "title": "test product",
            "description": "test description",
            "price": "120.0"
        }

        api_client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        resp = api_client.post(url, product_payload, format='json')
        assert resp.status_code == http_response

    @pytest.mark.parametrize(
        ["is_superuser", "http_response"],
        (
                (True, HTTP_200_OK),
                (False, HTTP_403_FORBIDDEN)
        )
    )
    @pytest.mark.django_db
    def test_update_product(self, api_client, products_factory, user_factory, token_factory, is_superuser,
                            http_response):
        user = user_factory(is_superuser=is_superuser)
        product = products_factory(_quantity=1)
        url = reverse('products-detail', args=[product[0].id])
        token = token_factory(user=user)

        product_payload = {
            "description": "update test description",
            "price": "200.00"
        }

        api_client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        resp = api_client.patch(url, product_payload, format='json')
        assert resp.status_code == http_response

    @pytest.mark.parametrize(
        ["is_superuser", "http_response"],
        (
                (True, HTTP_204_NO_CONTENT),
                (None, HTTP_401_UNAUTHORIZED)
        )
    )
    @pytest.mark.django_db
    def test_delete_product(self, api_client, user_factory, token_factory, products_factory, is_superuser,
                            http_response):
        user = user_factory(is_superuser=is_superuser)
        token = token_factory(user=user)
        product = products_factory(_quantity=1)
        url = reverse('products-detail', args=[product[0].id])
        api_client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        resp = api_client.delete(url)
        assert resp.status_code == http_response

    @pytest.mark.parametrize(
        ["search_field", "search_text", "http_response"],
        (
                ("title", "test_product", HTTP_200_OK),
                ("price", "100", HTTP_200_OK),
                ("description", "test_description", HTTP_200_OK),
        )
    )
    @pytest.mark.django_db
    def test_filter_products(self, api_client, products_factory, search_field, search_text, http_response):
        keys = {
            search_field: search_text
        }
        products = products_factory(_quantity=1, **keys)
        url = reverse('products-list')
        resp = api_client.get(url, params={search_field: search_text})
        assert resp.status_code == http_response
        assert len(resp.json()) == 1
