import pytest
from rest_framework.reverse import reverse
from rest_framework.status import HTTP_200_OK, \
    HTTP_201_CREATED, \
    HTTP_401_UNAUTHORIZED, \
    HTTP_204_NO_CONTENT


class TestOrderViewSet:

    @pytest.mark.parametrize(
        ["is_superuser", "http_response"],
        (
                (True, HTTP_200_OK),
                (None, HTTP_401_UNAUTHORIZED)
        )
    )
    @pytest.mark.django_db
    def test_retreive_order_list(self, api_client, user_factory, token_factory, is_superuser, http_response):
        user = user_factory(is_superuser=is_superuser)
        token = token_factory(user=user)
        url = reverse('orders-list')
        api_client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        resp = api_client.get(url)
        assert resp.status_code == http_response

    @pytest.mark.parametrize(
        ["is_superuser", "http_response"],
        (
                (True, HTTP_201_CREATED),
                (None, HTTP_401_UNAUTHORIZED)
        )
    )
    @pytest.mark.django_db
    def test_add_order(self, api_client, user_factory, token_factory, products_factory, is_superuser, http_response):
        user = user_factory(is_superuser=is_superuser)
        token = token_factory(user=user)
        url = reverse('orders-list')
        products = products_factory(_quantity=2)
        product_payload = {

            "customer": user.id,
            "status": "NEW",
            "positions": [{
                "quantity": 1,
                "product": products[0].id
            }],
        }

        api_client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        resp = api_client.post(url, product_payload, format='json')
        assert resp.status_code == http_response

    @pytest.mark.parametrize(
        ["is_superuser", "http_response"],
        (
                (True, HTTP_200_OK),
                (None, HTTP_401_UNAUTHORIZED)
        )
    )
    @pytest.mark.django_db
    def test_update_order(self, api_client, user_factory, token_factory, orders_factory, is_superuser, http_response):
        user = user_factory(is_superuser=is_superuser)
        token = token_factory(user=user)
        orders = orders_factory(_quantity=1)
        url = reverse('orders-detail', args=[orders[0].id])
        product_payload = {
            "status": "DONE",
        }

        api_client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        resp = api_client.patch(url, product_payload, format='json')
        assert resp.status_code == http_response

    @pytest.mark.parametrize(
        ["is_superuser", "http_response"],
        (
                (True, HTTP_204_NO_CONTENT),
                (None, HTTP_401_UNAUTHORIZED)
        )
    )
    @pytest.mark.django_db
    def test_delete_order(self, api_client, user_factory, token_factory, orders_factory, is_superuser, http_response):
        user = user_factory(is_superuser=is_superuser)
        token = token_factory(user=user)
        orders = orders_factory(_quantity=1)
        url = reverse('orders-detail', args=[orders[0].id])

        api_client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        resp = api_client.delete(url)
        assert resp.status_code == http_response


    @pytest.mark.parametrize(
        ["search_field", "search_text", "http_response"],
        (
                ("status", "NEW", HTTP_200_OK),
                ("total_amount", "10", HTTP_200_OK),
                ("created_at", '2021-06-23', HTTP_200_OK),
                ("updated_at", '2021-06-23', HTTP_200_OK),
                ("products", None, HTTP_200_OK),
        )
    )
    @pytest.mark.django_db
    def test_filter_orders(self, api_client, user_factory, token_factory, orders_factory, search_field, search_text,
                            http_response):
        user = user_factory(is_superuser=True)
        token = token_factory(user=user)
        if search_text is not None:
            keys = {
                search_field: search_text
            }
            orders = orders_factory(_quantity=1, **keys)
        else:
            orders = orders_factory(_quantity=1)
            search_text = orders[0].products.first().id
        url = reverse('orders-list')
        api_client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        resp = api_client.get(url, params={search_field: search_text})
        assert resp.status_code == http_response
        assert len(resp.json()) == 1
