from django.contrib import admin
from product_collections.models import ProductCollection


# Register your models here.

class ProductInline(admin.TabularInline):
    model = ProductCollection.selection.through  # проверить автодополнение IDE по этой строке
    extra = 1


@admin.register(ProductCollection)
class ProductCollectionAdmin(admin.ModelAdmin):
    inlines = [ProductInline]
    exclude = ('selection',)
