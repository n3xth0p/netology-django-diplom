from django_filters import rest_framework as filters
from product_collections.models import ProductCollection


class ProductCollectionFilter(filters.FilterSet):
    title = filters.CharFilter(field_name='title', lookup_expr='icontains')

    class Meta:
        model = ProductCollection
        fields = ('title',)
