from product_collections.models import ProductCollection
from product_collections.serializers import ProductCollectionSerializer
from product_collections.filters import ProductCollectionFilter
from rest_framework.viewsets import ModelViewSet
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.permissions import IsAdminUser, AllowAny


class ProductCollectionViewSet(ModelViewSet):
    queryset = ProductCollection.objects.all()
    serializer_class = ProductCollectionSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_class = ProductCollectionFilter

    def get_permissions(self):
        if self.action in ['create', 'destroy', 'update', 'partial_update', ]:
            return [IsAdminUser()]
        return []
