from django.db import models
from products.models import CommonDateInfoModel, Product


# Create your models here.

class ProductCollection(CommonDateInfoModel):
    title = models.CharField(max_length=256, verbose_name='Заголовок')
    description = models.TextField(verbose_name='Текст подборки')
    selection = models.ManyToManyField(Product, verbose_name='Товары подборки')

    class Meta:
        verbose_name = 'Подборка'
        verbose_name_plural = 'Подборки'

    def __str__(self):
        return f"id: {self.id}, Заголовок: {self.title}"
