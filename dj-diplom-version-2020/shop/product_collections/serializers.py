from rest_framework import serializers
from product_collections.models import ProductCollection


class ProductCollectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductCollection
        fields = '__all__'
