from django_filters import rest_framework as filters, DateTimeFromToRangeFilter
from reviews.models import Review


class ReviewFilter(filters.FilterSet):
    """Фильтры для обзоров."""

    created_at = DateTimeFromToRangeFilter()

    class Meta:
        model = Review
        fields = ['customer', 'product', 'created_at']
