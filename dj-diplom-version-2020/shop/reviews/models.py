from django.db import models
from products.models import CommonDateInfoModel, Product
from django.contrib.auth.models import User


# Create your models here.
class Rating(models.IntegerChoices):
    """" Варианты оценки товара в отзыве"""
    ONE_STAR = 1
    TWO_STAR = 2
    THREE_STAR = 3
    FOUR_STAR = 4
    FIVE_STAR = 5


class Review(CommonDateInfoModel):
    """" Отзыв на товар"""

    class Meta:
        verbose_name = "Отзыв"
        verbose_name_plural = "Отзывы"
        unique_together = ["customer", "product"]

    customer = models.ForeignKey(User, on_delete=models.DO_NOTHING, verbose_name="ID автора отзыва",
                                 related_name="reviews")
    product = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name="Товар", related_name="reviews")
    text = models.TextField(verbose_name="Текст")
    rating = models.IntegerField(choices=Rating.choices)
