from rest_framework import serializers
from reviews.models import Review


class ReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = ('id', 'customer', 'product', 'text', 'rating')
        read_only_fields = ['customer']

    def create(self, validated_data):
        validated_data["customer"] = self.context["request"].user
        return super().create(validated_data)

    def validate(self, value):
        user = self.context["request"].user
        product = value.get("product")

        if Review.objects.filter(customer=user, product=product).exists():
            raise serializers.ValidationError('Нельзя оставлять более одного отзыва на товар.')
        return value
