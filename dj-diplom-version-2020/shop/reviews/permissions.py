from rest_framework.permissions import BasePermission


class IsOwnerOrAdmin(BasePermission):
    """ Проверка на Owner или Admin"""

    def has_object_permission(self, request, view, obj):
        return obj.customer == request.user or request.user.is_staff


class DenyAny(BasePermission):
    """ Закрыть доступ """

    def has_permission(self, request, view):
        return False

    def has_object_permission(self, request, view, obj):
        return False


class IsOwnerUser(BasePermission):
    """ Проверка на Owner """

    def has_object_permission(self, request, view, obj):
        return obj.customer == request.user
