from django.contrib import admin
from orders.models import Order, Position


class PositionInline(admin.TabularInline):
    model = Position
    extra = 1


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    inlines = [PositionInline]
    ordering = ('-created_at',)
    readonly_fields = ('total_amount', 'customer', 'created_at', 'updated_at')


@admin.register(Position)
class PositionAdmin(admin.ModelAdmin):
    pass
