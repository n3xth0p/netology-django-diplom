from orders.models import Order, Position, Status
from rest_framework import serializers


class PositionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Position
        fields = ('id', 'product', 'quantity')


class OrderSerializer(serializers.ModelSerializer):
    positions = PositionSerializer(many=True, source='position_order')

    class Meta:
        model = Order
        fields = (
            'id', 'customer', 'status', 'total_amount', 'created_at', 'updated_at', 'positions'
        )
        read_only_fields = ('customer',)

    def create(self, validated_data):
        positions = validated_data.pop('position_order')
        print(positions)
        order = Order.objects.create(customer=self.context['request'].user, status=Status.NEW)

        total_order_amount = 0
        for position in positions:
            Position.objects.create(order=order, product=position['product'], quantity=position['quantity'])
            total_order_amount += position['quantity'] * position['product'].price
        order.total_amount = total_order_amount
        order.save()

        return order

    def update(self, instance, validated_data):
        instance.status = validated_data['status']
        instance.save()
        return instance

    def validate_positions(self, value):
        for position in value:
            quantity = position.get('quantity')
            if quantity <= 0:
                raise serializers.ValidationError('Количество товаров должно быть больше 0')
        return value
