from django_filters import rest_framework as filters, DateTimeFromToRangeFilter
from orders.models import Order


class OrderFilter(filters.FilterSet):
    """Фильтры для заказов."""

    created_at = DateTimeFromToRangeFilter()
    updated_at = DateTimeFromToRangeFilter()

    class Meta:
        model = Order
        fields = ('status', 'created_at', 'updated_at', 'total_amount')
