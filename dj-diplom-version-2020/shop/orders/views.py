from rest_framework.viewsets import ModelViewSet
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from reviews.permissions import IsOwnerOrAdmin
from orders.filters import OrderFilter
from orders.models import Order
from orders.serializers import OrderSerializer


# Create your views here.
class OrderViewSet(ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_class = OrderFilter

    def get_permissions(self):
        if self.action in ["retrieve"]:
            return [IsOwnerOrAdmin()]
        if self.action in ["create", "list"]:
            return [IsAuthenticated()]
        if self.action in ["update", "partial_update", "destroy"]:
            return [IsAdminUser()]
        return []

    def get_queryset(self):
        customer = self.request.user
        if customer.is_staff:
            return Order.objects.all()

        return Order.objects.filter(customer=customer.id)
