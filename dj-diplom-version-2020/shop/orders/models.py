from django.db import models
from products.models import CommonDateInfoModel, Product
from django.contrib.auth.models import User


# Create your models here.

class Status(models.TextChoices):
    """Статусы заказов"""
    NEW = "NEW", "Новый"
    IN_PROGRESS = "IN_PROGRESS", "В работе"
    DONE = "DONE", "Выполнен"


class Order(CommonDateInfoModel):
    """Заказы"""

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    def __str__(self):
        return f'OrderId:{self.id}; Сумма заказа:{self.total_amount};'

    customer = models.ForeignKey(User, on_delete=models.DO_NOTHING, verbose_name='ID пользователя')
    status = models.CharField(choices=Status.choices, verbose_name="Статус", max_length=20, default=Status.NEW)
    total_amount = models.DecimalField(verbose_name="Сумма заказа", editable=False, default=0, decimal_places=2,
                                       max_digits=10)
    products = models.ManyToManyField(Product, through="Position")


class Position(models.Model):
    """ Позиции заказа"""

    class Meta:
        verbose_name = "Позиция заказа"
        verbose_name_plural = "Позиции заказа"

    order = models.ForeignKey(Order, verbose_name="ID Заказа", on_delete=models.CASCADE, related_name="position_order")
    product = models.ForeignKey(Product, verbose_name="ID Товара", on_delete=models.CASCADE)
    quantity = models.IntegerField(verbose_name="Количество единиц", default=1)

    def __str__(self):
        return f'PositionId:{self.id}; OrderId:{self.order.id}, ProductId:{self.product}, Количество:{self.quantity}'
