# Проект Web API для Интернет-магазина

##### Для запуска выполнить комманды:


- установить зависимости: ```pip install -r requirements.txt```
- подготовить БД и настройки к ней в ```settings.py```
- сделать миграции ```python manage.py migrate```
- загрузить тестовые данные в БД (при необходимости): ```python manage.py loaddata initial.json```
- запустить сервер ```python manage.py runserver```